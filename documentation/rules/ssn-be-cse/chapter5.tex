% Chapter 4

\chapter{Problem Definition and Proposed System} % Write in your own chapter title


{\bf Finite Automata} are the simplest mathematical abstraction of {\bf discrete sequential systems} such as computers. A finite automaton consists of states and transitions defined over an input alphabet. When an automaton sees a symbol of input, it makes a transition (or jump) to another state, according to its transition function (which takes the current state and the recent symbol as its inputs). A set of initial states as well as final states are also defined such that the automaton starts its computation from an initial state for the computation to be legal. When a legal computation ends in a final state it is desirable otherwise not.

\section{Nondeterministic Finite Automata}
Automata, generally, come in two variations: Nondeterministic Finite Automata (\ac{NFA}) and Deterministic Finite Automata (\ac{DFA}). In an NFA, given a state and a symbol, the machine can make a transition to one or more states, whereas in a DFA the choice is limited to exactly one state. NFAs and DFAs are equivalent formal models. For every NFA $A$ we can show that there exists an equivalent DFA $A'$ and vice versa. We say that two automata (whether DFA or NFA) are equivalent if they admit the same language.


The use of nondeterministic finite automata in natural language processing \cite{Moh97} and model checking \cite{CGP},\cite{CGL} is well-known. NFAs provide an intuitive model for many natural language problems and also for model checking, and in addition have potentially fewer states than deterministic finite automata.

Formally, a nondeterministic finite automaton (NFA) is a quintuple $A=(Q,\Sigma,\Delta,I,F)$ where
\begin{itemize}
\item $Q$ is a finite set of states 
\item $\Sigma$ is a finite set of input symbols 
\item $\Delta$ is the transition function that takes a state $q$ in $Q$ and an input symbol $a$ in $\Sigma$ as arguments and returns a subset of $Q$. That is, $\Delta:Q \times \Sigma \to 2^Q$.
\item $I\subseteq Q$ is the set of initial states and  
\item $F \subseteq Q$ is called the set of final states.
\end{itemize}
For every state $q$ and symbol $a$ if $|\Delta(q,a)| \le 1$ then NFA is essentially deterministic. Clearly, it means that  DFAs are a subclass of NFAs.

The computations ({\bf run}) of an NFA $A$, as given above, are defined as follows. A {\bf run} of $A$ over a word $w=a_1a_2\cdots a_n\in \Sigma^*$ is a sequence $\rho= Iq_1q_2 \cdots q_n$ where $I \in Q$ and $q_i \in \Delta(q_{i-1},a_i)$ for all $i\ge 1$. A run $\rho$ is accepting if $q_n \in F$. A word $w$ is accepted by $A$ if there is an accepting run of $A$ over $w$. The language of words accepted by $A$ is denoted by $Lang(A)$.


It turns out that an NFA $A$ can exhibit several different runs for a given input word $w$. $w$ is accepted by $A$ if and only if at least one of the possible runs, defined by the input, leads to a final state. On the other hand a DFA has exactly one run for input word $w$. If that run ends in a final state then $w$ accepted otherwise $w$ is rejected.

As language recognizer devices, NFAs are no more powerful than their deterministic counterpart, indeed they accept the same class of languages, that is to say they are equivalent. However, NFAs are very often more convenient to use. In many cases, the most direct formalization of a problem, as in NLP and Model Checking, is in terms of NFAs.

As an example we give an NFA in Figure \ref{fig:CR}. This NFA having three states accepts all strings from $\{0,1\}^*$ which end in ``01''. Figure \ref{fig:CR} is a pictorial representation of the following NFA quintuplet $A=(Q,\Sigma,\Delta,I,F)$ where 
\begin{itemize}
\item $Q=\{q_0,q_1,q_2\}$,
\item $\Sigma=\{0,1\}$,
\item $\Delta:Q\times\Sigma \to 2^Q$ is given as follows:
\[\Delta(q_0,0)=\{q_0,q_1\},\Delta(q_0,1)=\{q_0\},\Delta(q_1,1)=\{q_2\},\]\[\Delta(q_1,0)=\Delta(q_2,0)=\Delta(q_2,1)=\emptyset,\]
\item $I=\{q_0\}$ and
\item $F=\{q_2\}$.
\end{itemize}
\begin{figure}
\begin{center}
\input{figure1}
\caption{NFA to accept all strings terminating in ``01''}
\label{fig:CR}
\end{center}
\end{figure}

\subsection{Applications of NFA}
Applications of NFA include various aspects of {\bf natural language processing} (NLP). NFA may be used to store sets of words, with or without annotations such as the corresponding pronunciation, base form, or morphological categories. The
main reasons for using NFA in the NLP domain are that their representation of the set of words is compact, and that looking up a word $w$ in a dictionary represented by an NFA is very fast--proportional to the length of $w$, that is, $\mathbf{O}(|w|)$.
 
NFAs are heavily used in {\bf model checking}. In model checking a given hardware or software system is checked whether it meets certain specifications. This check must be exhaustive and automatic. Typically, the system is described as a finite labelled transition system (a variation of NFA) and the specifications are coded in a logical language. In automata based model checking, given by Vardi and Wolper \cite{VW}, the system is described as an NFA $A$ and the negation of the logical specification ($\alpha$) can be automatically converted to another NFA $A_{\lnot\alpha}$. Thereafter, the intersection of the two NFAs ($A \cap A_{\lnot\alpha}$) is checked for language emptiness. If the language of the NFA $A\cap A_{\lnot\alpha}$ is empty then the  system $A$ satisfies the property $\alpha$ otherwise not.

\subsection{NFA determinisation and DFA minimization}
Subset construction \cite{2} is the method of converting a NFA into a DFA. The DFA equivalent to the NFA in the figure \ref{fig:CR} is given in the figure \ref{fig:dfa}. In this case, the NFA and equivalent DFA have the same number of states. In general, the size of equivalent DFA is exponential in the size of NFA. 
\begin{figure}
\begin{center}
\input{figure4}
\caption{DFA to accept all strings terminating in ``01''.}
\label{fig:dfa}
\end{center}
\end{figure}

Consider the NFA in figure \ref{fig:exp-nfa}. This NFA accepts all those strings over $\Sigma=\{0,1\}$ which has $1$ as its third last letter. This NFA has $4$ states.
\begin{figure}
\begin{center}
\input{figure5}
\caption{NFA to accept all strings with third last letter as ``1''.}
\label{fig:exp-nfa}
\end{center}
\end{figure}
The equivalent DFA, given in figure \ref{fig:exp-dfa} has $8$ states. In general, we can argue that for any such NFA which accepts strings with $n$th last letter as $1$ has $\mathbf{O}(2^n)$ states.
\begin{figure}
\begin{center}
\input{figure6}
\caption{DFA to accept all strings with third last letter as ``1''.}
\label{fig:exp-dfa}
\end{center}
\end{figure}




NFAs are an appropriate model for sequential systems. Unfortunately, they are quite deficient in order to model distributed systems with multiple agents. We find many different automata-based models for distributed, in particular message-passing, systems in the literature. The most famous one, called communicating finite-state machines (CFSMs) \cite{BZ} is by Brand and Zafiropulo, which has many variants, including message passing automata (MPA) \cite{HMKST05}. We look at another variant of CFSMs, namely systems of communicating automata (SCA). 
\section{System of Communicating Automata}
%
Systems of Communicating Automata (\ac{SCA}) were introduced in \cite{7}
and the presentation here follows \cite{7} and \cite{Mee}.

We focus our attention on $n$-agent systems, captured by $n$-SCAs. 
A {\sf distributed alphabet} for such systems is an $n$-tuple $\DA$, 
where for each $i \in [n]$, $\Sigma_i$ is a finite non-empty alphabet of 
actions of agent $i$ and for all $i \neq j$, $\Sigma_i \cap \Sigma_j = 
\emptyset$. The alphabet induced by $\DA$ is given by $\Sigma = \cupover_i 
\Sigma_i$. The set of {\sf system actions} is the set $\Sigma' = 
\{\lambda\} \cup \Sigma$. The action symbol $\lambda$ is referred to as 
the {\sf communication action}. This is used as an action representing 
a communication constraint through which every receive action will be 
dependent on its corresponding send action. We use $a, b, c$ {\it etc.}, to 
refer to elements of $\Sigma$ and $\tau, \tau'$ {\it etc.}, to refer to those 
of $\Sigma'$.

\begin{dfn} 
A {\bf System of $n$ Communicating Automata (SCA)} on a distributed alphabet 
$\DA$ is a tuple $S = ((Q_1,G_1), \ldots, (Q_n,G_n) \to, Init)$ where,

\begin{enumerate}

  \item For $j \in [n]$, $Q_j$ is a finite set of (local) states of agent $j$. 
  \newline 	
  For $j \neq j'$, $Q_j \cap Q_{j'} = \emptyset$. 

  \item $Init \subseteq (Q_1 \times \ldots \times Q_n)$ is the set of 
  (global) initial states of the system. 

  \item for each $j \in [n]$, $G_j \subseteq Q_j$ is the set of
  (local) good states of agent $j$.

  \item Let $Q = \cupover_j Q_j$, then, the transition relation $\to$
  is defined over $Q$ as follows. $\to \subseteq (Q \times \Sigma' 
  \times Q)$ such that if $q \step{\tau} q'$ then either there exists 
  $j$ such that $\{q, q'\} \subseteq Q_j$ and $\tau \in \Sigma_j$, or 
  there exist $j \neq j'$ such that $q \in Q_j, q' \in Q_{j'}$ and 
  $\tau = \lambda$. 

\end{enumerate}
\end{dfn}

Thus, SCAs are systems of $n$ finite state automata with $\lambda$-labelled
communication constraints between them. Note that $\to$ above is {\it not} 
a global transition relation, it consists of {\sf local transition 
relations}, one for each agent, and {\sf communication constraints} 
of the form $q \step{ \lambda} q'$, where $q$ and $q'$ are states of 
different agents. The latter define a coupling relation rather than 
a transition. The interpretation of local transition relations is standard: 
when the agent $i$ is in state $q_1$ and reads input $a \in \Sigma_i$, it 
can move to a state $q_2$ and be ready for the next input if $(q_1, a, q_2) 
\in \to$. The interpretation of communication constraints is non-standard 
and depends only on automaton states, not on local input. When $q \step{ \lambda}
q'$, where $q \in Q_i$ and $q' \in Q_j$, it constrains the system 
behaviour as follows: whenever agent $i$ is in state $q$, it puts
a message whose content is $q$ and intended recipient is $j$ into the 
buffer; whenever agent $j$ intends to enter state $q'$, it checks its 
environment to see if a message of the form $q$ from $i$ is available for 
it, and waits indefinitely otherwise. If a system $S$ has no $\lambda$ 
constraints at all, automata  proceed asynchronously and do not 
wait for each other. We will refer to $\lambda$-constraints as 
`$\lambda$-transitions' in the sequel for uniformity, but this 
explanation (that they are constraints not dependent on local input) should
be kept in mind. 


We  use the notation $\presca{q} \defn \{q' \mid q' \step{\lambda}
q\}$ and $\postsca{q} \defn \{q' \mid q \step{\lambda} q'\}$. For $q \in Q$, 
the set $\presca{q}$ refers to the set of all states from which $q$ has 
incoming $\lambda$-transitions and the set $\postsca{q}$ is the set of all 
states to which $q$ has outgoing $\lambda$-transitions. 
%
{\it Global behaviour} of an SCA will be defined using its set of 
{\sf global states}. To refer to global states, we will use the set 
$\widetilde{Q} \defn (Q_1 \times \dots \times Q_n)$. When $u = (q_1, \dots, 
q_n) \in \widetilde{Q}$, we use the notation $u[i]$ to refer to $q_i$.

Figure~\ref{fig:fig:sca-eg} gives an SCA over the alphabet $\Sigtil = 
(\{a\}, \{b\})$ (We use $\Rightarrow$ to mark the initial states). 
The (global) initial states and (global) good states of this SCA
are $\{(s_0,t_0)\}$. The reader will observe that this SCA
models the producer-consumer protocol. The producer generates an object
via a $s_0\step{a}s_0$ transition, whereas the consumer consumes an
object through a $t_0\step{b}t_0$ transition. As a consumption can follow
only after a production there is a $\lambda$ transition between $s_0$ and 
$t_0$. Now, it is not difficult to see why $(s_0,t_0)$ is an initial state 
as well as final state. The producer can always terminate in $s_0$ after 
generating zero or more objects and consumer can terminate in $t_0$ 
after consuming one or more objects.

\begin{figure*}
\begin{center}
\begin{tikzpicture}[scale=0.75]
	\node 	(e0)		at	(-2,0) 	[rectangle, thick]{};
	\node 	(s0)		at	(0,0) 	[circle, thick, draw=blue!50,fill=blue!20,inner sep=0pt,minimum size=1cm]{$s_0$};


	\node 	(t0)		at	(6,0) 	[circle, thick, draw=blue!50,fill=blue!20,inner sep=0pt,minimum size=1cm]{$t_0$};

	\node 	(f0)		at	(8,0) 	[rectangle, thick]{};
	\draw[->,very thick, bend left=40]	(s0)	to	node[auto]{$\lambda$}	(t0);

	\draw[->,very thick] 		(f0) to 	node[auto]{} 			(t0);
	\draw[->,very thick] 		(e0) to 	node[auto]{} 			(s0);
	\draw[loop above,very thick] 	(s0) to 	node[auto]{$a$} 		(s0);
	\draw[loop above,very thick] 	(t0) to 	node[auto]{$b$} 		(t0);
\end{tikzpicture}
\end{center}
\caption{A simple SCA}
\label{fig:fig:sca-eg}
\end{figure*}

%
The language accepted by an SCA is a collection of ($\Sigma$-labelled) 
Lamport diagrams.
\section{Lamport Diagrams}
%
We know that behaviours of sequential systems can be described by 
finite or infinite words over a suitable alphabet of actions. 
The system has an underlying set of {\sf events} and an alphabet 
of {\sf actions} that label the {\sf event occurrences}. A 
word over such an alphabet represents a behaviour of the system 
as a totally ordered sequence of actions of the system and a 
set of such words represents possible behaviours of the system. 
Extending this intuition, Lamport \cite{Lam78} suggested that we 
could use partial orders to represent computations of concurrent
systems. Since event occurrences of different agents can be
independent of each other, events of the system are partially
ordered. {\sf Lamport diagrams}, representing non-sequential runs,
are partial orders with the underlying set of events partitioned 
into those of $n$ agents in such a way that the event occurrences 
of every agent form a linear order. The ordering relation 
captures the {\sf causal dependence} of event occurrences.
Lamport Diagrams were first defined formally in
\cite{7}, the discussion here is taken from \cite{Mee}. 
We fix the number of agents in the system as $n$ and take
$[n]=\{1,2,\cdots,n\}$.
A {\sf distributed alphabet} for such systems is an $n$-tuple $\DA$, 
where for each $i \in [n]$, $\Sigma_i$ is a finite non-empty alphabet of 
actions of agent $i$ and for all $i \neq j$, $\Sigma_i \cap \Sigma_j = 
\emptyset$. The alphabet induced by $\DA$ is given by $\Sigma = \cupover_i 
\Sigma_i$. 
%
We define ($\Sigma$-labelled) Lamport diagrams formally, as follows:
\begin{dfn}
A {\bf Lamport diagram} is a tuple $D=(E, \leq, V)$ where 
\begin{itemize}

 \item $E$ is an at most countable set of {\sf events}.

 \item $\leq \subseteq (E \times E)$ is a partial order called the 
 {\sf causality relation} such that for every $e \in E$, 
 $\down e \defn \{ e' \in E \mid e' \leq e \}$ is finite. 

 \item $V:E \rightarrow \Sigma$ is a {\sf labelling function} which satisfies
 the following condition: 

 Let $E_i \defn \{e \in E \mid V(e) \in \Sigma_i\}$ and $\leq_i \defn \leq \cap
 (E_i \times E_i)$. then for every $i \in [n]$, $\leq_i$ is a total order
 on $E_i$.
\end{itemize}
\end{dfn}
%
\input{cl-ser-ex}
In the above definition, the relation $\leq$ describes the 
causal dependence of events and the relations $\{ \leq_i \mid 
i \in [n] \}$ capture the fact that event occurrences of each 
agent are totally ordered. Note that the labelling function 
$V$ implicitly assigns a unique agent to every event. For example,
if $V(e)\in \Sigma_i$ for some $e\in E$ and $i\in [n]$, then the
event $e$ belongs to the agent $i$. This, in turn, rules out any
synchronous communication in the underlying system, as, in that case,
a synchronous or joint communication event occurrence would be associated 
with more than one agent.
% 

%
%
For example, consider a system where a fixed finite set of 
clients are registered with a server that provides them access 
to a database. A Lamport diagram representing a behaviour of 
the system is depicted in Figure ~\ref{fig:fig:client-server-ld}. There 
are four agents: {\sf client1} and {\sf client2} are two 
clients registered with the {\sf server} in order to access 
the {\sf database}. Event $e_1$ is an event occurrence of 
the agent {\sf client1} corresponding to sending of the 
message {\sf request1} to the server. The receipt of this 
message by the {\sf server} is represented by the event 
occurrence $e_6$. The server passes the requests to the 
{\sf database} (represented by {\sf lookup1} and {\sf 
lookup2}) and the response from the {\sf database} 
({\sf data1} and {\sf data2}) is communicated back to 
the clients. Observe that event occurrences $e_1$ and 
$e_3$ corresponding to sending of requests from {\sf client1} and 
{\sf client2} respectively are {\it concurrent}, {\it i.e.}, 
they are not causally dependent on each other. On the 
other hand, the event occurrences $e_5$ and $e_6$ corresponding to 
the receipt of the messages {\sf request1} and {\sf request2} 
respectively, are causally dependent. Thus, {\sf request1} and 
{\sf request2} are concurrently originating but sequentialized by 
the server computation. Similarly, for $e_{13}$ to occur, $e_7$ and 
$e_1$ should have already occurred. It is in this sense 
that Lamport diagrams depict the causal dependence of 
various event occurrences within a system computation. 

To be precise, the relation $\leq$ is causal in the 
sense that whenever $e \leq e'$, we interpret this as 
the condition that, in any run of the system, $e'$ cannot 
occur without $e$ having occurred previously in that 
run. Since for all $e \in E$, $\down e$ is finite, $\leq$ 
must be discrete. Hence there exists $\lessdot \subset 
\leq$, the {\sf immediate causality relation}, which 
generates the causality relation; that is: for all 
$e, e'$, $e \lessdot e'$ iff $e <e'$ and for all $e''
\in E$ if $e \le e''\le e'$ then either $e''= e$ or $e''=e'$. We have 
$\leq = (\lessdot)^*$. Now consider 
$e \lessdot e'$. If $e, e' \in E_i$ for some $i \in [n]$, we 
see this as {\sf local causal dependence}. However, if 
$e \in E_i$ and $e' \in E_j$, $i, j \in [n]$, $i \neq j$, 
we have {\sf remote causal dependence}. For $e, e' \in E$, 
define $e <_c e'$ iff $e \in E_i$, $e' \in E_j$, $i \neq j$ 
and $e \lessdot e'$. In this case, we interpret $e$ as the 
sending of a message by agent $i$ and $e'$ as its 
corresponding receipt by $j$. Accordingly, if $e <_c e'$ 
then $e$ will be referred to as a {\sf send event} and 
$e'$ will be its corresponding {\sf receive event}. An event 
$e$ will be interpreted as a {\sf local event} if there exists no 
$e'$ such that $e <_c e'$ or $e' <_c e$. Notice that the 
communication relation $<_c$ is derived from the Hasse diagram 
of the causal dependence relation which is a partial order. 

Note that given an event $e \in E$, there can be at most 
$n$ events $e'$ such that $e \lessdot e'$ and at most $n$ 
events $e'$ such that $e' \lessdot e$. In particular, if 
$e \in E_i$ and $e <_c e'$, $e <_c e''$ where $e' \in E_j$ 
and $e'' \in E_k$ for $j, k \in [n]$ such that $j \neq i$ 
and $k \neq i$, then $e$ is a send event {\sf simultaneously} 
to agents $j$ and $k$. Such events can be thought of as 
representing broadcast type of communication where a common 
message is broadcast to several agents in the system. 
Similarly, there can be events which are simultaneous receive 
events from more than one agent. Also, an event $e$ 
can be a send and a receive event simultaneously. For 
example, the events $e_9$ and $e_{10}$ in the Lamport 
diagram given in Figure ~\ref{fig:fig:client-server-ld} are events 
which represent send and receive actions simultaneously. 


\section{Problem Definition}
As we mentioned in the first chapter, BDDs can be used to efficiently represent large sets. The use of OBDDs in model checking allows systems with large state spaces to be verified. That is, NFAs which are generated in a model checking system can by symbolically represented as ROBDDs, instead of concrete state space systems \cite{BCMD},\cite{CGL92},\cite{McM93}.

Consider model checking of distributed systems which generate SCAs in their wake. The challenge is to model SCAs as ROBDDs which we want to take up as a part of this project. 

Our proposed system will take an arbitrary SCA $A$ as input and return an equivalent ROBDD $D$. This system can be used as a plugin for the model checking tool for distributed systems.

\subsection{Detailed design}
The detailed design of our project is presented in the figure \ref{fig:SCA}. As shown in the figure, the input to the system is an SCA which is $3n+2$-tuple, $\langle (S_1,\to_1,G_1),\cdots (S_n,\to_n,G_n),\step{\lambda},\widetilde{I}\rangle$, where $n$ is the number of agents in the distributed system modelled by the given SCA.
\begin{figure}[hbt] 
{\centering {\includegraphics[width=4in,height=6in]{./figures/designbdd2}}\par}
\caption{\label{fig:SCA} System flow Diagram}
\end{figure}

For every $1\le i\le n$, for every local state set $S_i$ as well as local good state $G_i$, we construct the equivalent BDDs $B_{S_i}$ and $B_{G_i}$, via the routine $set\_to\_BDD$. For every $1\le i\le n$, and for every $\to_i$ as well as $\step{\lambda}$, we construct equivalent BDDs $B_{\to_i}$ and $B_{\step{\lambda}}$ via the routine $set\_pair\_BDD$. The global initial state $\widetilde{I}$ is converted to equivalent BDD $B_{\widetilde{I}}$. 

Note, we have separate BDD-converson routines for state sets and transition relations, as they are structurally different. Furthermore, $\widetilde{I}$ needs a different routine for conversion to BDD form as it contains $n$-tuples.

We define another set of routines which compute the set $pre_{\exists}(X,\to)$ and $pre_{\forall}(X,\to)$ (as BDDs $B_{\exists},B_{\forall}$), respectively, for any subset of states $X$ and transition relation $\to$. These sets may be useful when we intend do reachability analysis on the input SCA for checking satisfiability, or during model checking.

These BDDs may be combined together and given as output in the form of a high level BDD.
