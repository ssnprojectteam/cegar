% Chapter 3

\chapter{Algorithms for BDD}
The various algorithms for BDDs depend on a simple technique called {\bf Shannon expansion}. It is used to convert any boolean function to an equivalent if-then-else normal form (INF). The BDD for a boolean function $f$ is obtained from the corresponding INF of $f$. 

\section{Shannon Expansion}
%
Shannon expansion is the identity : $f = x \cdot f[1/x] + \bar{x}\cdot \and f[0/x]$ where $f$ is a boolean function and $f[1/x]$ is the boolean function obtained by replacing $x$ in $f$ by $1$ whereas $f[0/x]$ is the boolean function obtained by replacing $x$ in $f$ by $0$.

Shannon expansion is based on a simple observation. If $f$ is a boolean function and $x$ is a variable in $f$ then $f$ is equivalent to $x \cdot f[1/x] + \bar{x}\cdot \and f[0/x]$. This is easy to see. When $x=0$, the function is equal to $0\cdot f[1/x]+1\cdot f[0/x]$, which is $f[0/x]$. When $x=1$, the function is equal to $1\cdot f[1/x]+0\cdot f[0/x]$, which is $f[1/x]$.

Although this observation is known as Shannon expansion, it was first enunciated in G. Boole's 1854 book ``The Laws of Thought''.

\section{If-Then-Else Normal Form}
An If-Then-else Normal Form (INF) is a Boolean expression built entirely from the {\bf if-then-else} operator and the constants $0$ and $1$ such that all tests are performed only on variables. The if-then-else operator is a tertiary operator defined as follows:
%
\[x \to y_0,y_1=(x ~\& ~ y_0)|(\lnot x ~\& ~  y_1)\]
We use $\&$ for $\cdot$ (logical and) and $|$ for $+$ (logical or). It turns out that every logical operator can be defined in terms of if-then-else operator.
\begin{itemize}
\item $\lnot x \equiv (x\to 0,1)$, 
\item $x~\& ~y \equiv x \to(y \to 1,0),(y \to 0,0)$,
\item $x ~| ~y \equiv x \to(y \to 1,1),(y \to 1,0)$,
\item $x\To y \equiv x \to(y \to 1,0),(y \to 1,1)$,
\item $x\Leftrightarrow y \equiv x \to(y \to 1,0),(y \to 0,1)$. 
\end{itemize}
Since variables must only occur in tests the Boolean expression $x$ is represented as $ x \to 1, 0$. Since, every logical operator can be described using if-then-else operator, we can show that every boolean expression (propositional formula) can be converted to an equivalent INF expression.

As we mentioned earlier, Shannon expansion is used to generate an INF from any expression $t$. If $t$ contains no variables it is either equivalent to $0$ or $1$ which is an INF. Otherwise we form the Shannon expansion of $t$ with respect to one of the variables $x$ in $t$.

\section{Examples} Consider the Boolean expression $t = (x1 ~\& ~y1) | (x2 ~\& ~y2)$. We can convert this expression to an equivalent INF form with order $x_1,y_1, x_2, y_2$. This is the order in which we perform Shannon expansions on $t$.

$t=(x_1~\& ~y_1)|(x_2~\& ~y_2)$

$t= x_1\to t_1,t_0$

$t_0= y_1\to t_{01},t_{00}$ 

$t_1= y_1\to 1,t_{10}$

$t_{01}= x_2\to t_{011},0$

$t_{00} =x_2\to t_{001},0$ 

$t_{10} =x_2\to t_{101},0$

$t_{011} =y_2\to 1,0$ 
 
$t_{001}=y_2\to 1,0$

$t_{101}=y_2\to 1,0$

The Shannon expansion of $t$ can be directly transformed to the equivalent decision tree of $t$ as shown in the figure \ref{fig:SE1}. After merging the terminals and performing appropriate reductions we obtain the equivalent BDD for $t$ as shown in figure \ref{fig:SE2}.
\begin{figure}[h] o
{\centering {\includegraphics[width=4in,height=6in] {./figures/shannon.png}}}
\caption{\label{fig:SE1}Decision Tree for  $t=(x_1\&y_1)|(x_2\&y_2)$.}
\end{figure}

%BDDs – directed acyclic graph of Boolean expressions. If the variables occur in the same ordering on all paths from root to leaves, we call this OBDD.
%Example: $t=(x_1\&y_1)|(x_2\&y_2)$
%Shannon expansion of t in order x1,y1,x2,y2

%  $t            =x_1\to t_1,t_{01}$

%  $t_1        =y_1\to 1,t_{01}$

%  $t_{01}   =x_2\to t_{011},0$

%  $t_{011} =y_2\to 1,0$


\begin{figure}[h] 
{\centering {\includegraphics[width=4in,height=6in] {./figures/sh1.png}}}
\caption{\label{fig:SE2}ROBDD for  $t=(x_1\&y_1)|(x_2\&y_2)$.}
\end{figure}



\section{The algorithm reduce}
An OBDD is called reduced OBDD if satisfy the following property:
\begin{enumerate}
\item{Uniqueness :} There are no two distinct nodes testing the same variable
with the same successors.
\item{Irredundancy:} The low and high successors of every node are distinct.
\end{enumerate}
The reductions $C1$ and $C3$ are at the core of any serious use of OBDDs, construct a BDDs want to convert it to its reduced form. Here, we  describe an algorithm called {\it reduce} which does this efficiently for ordered BDDs.
If the ordering of $B$ is $[x_1, x_2, . . . , x_l]$, then B has at most $l+1$ layers. The
algorithm reduce now traverses $B$ layer by layer in a bottom-up fashion,
beginning with the terminal nodes. In traversing $B$, it assigns an integer
label $id(n)$ to each node $n$ of $B$, in such a way that the subOBDDs with
root nodes $n$ and $m$ denote the same boolean function if and only if $id(n)$
equals $id(m)$.
Since reduce starts with the layer of terminal nodes, it assigns the first
label (say \#0) to the first $0$-node it encounters. All other terminal $0$-nodes
denote the same function as the first $0$-node and therefore get the same label. Similarly, the $1$-nodes all get the next label,
say \#1.
Given OBDD, reduce proceeds bottom-up assigning an integer label, $id(n)$ for each node :
% (1) if id(lo(n)) = id(hi(n) then id(n) = id(lo(n)).
% (2) if there is another node for x_i, say m such that id(lo(n) = id(lo(m)) and id(hi(n)) = id(hi(m)), then id(n) = id(m).
% (3) otherwise we set id(n) to the next node unused integer.

\section{The Algorithm apply}
In ROBDDs the algorithm apply is the most important one. All the binary operators on ROBDDs are implemented by a general algorithm $apply(op,B_f,B_g)$, where $B_f$ and $B_g$ are ROBDDs for boolean functions $f$ and $g$ and $op$ is any operation defined on boolean expressions such as $+,\cdot$ {\it etc}. The algorithm operates recursively on the structure of the two ROBDDs:
\begin{enumerate}
\item let $x$ be the variable highest in the ordering (leftmost in the list) which occurs
in $B_f$ or $B_g$, then
\item split the problem into two subproblems for $x$ being $0$ and $x$ being $1$ and solve
recursively;
\item at the leaves, apply the boolean operation $op$ directly.
\end{enumerate}

\section{The Algorithm restrict}
\begin{definition}
Let $f$ be the Boolean formula and $x$ be the Boolean variable it is denoted by $f[0/x]$ obtained by replacing all occurrences
of $x$ in $f$ by $0$. The formula $f[1/x]$ is defined similarly. The expressions $f[0/x]$
and $f[1/x]$ are called {\it restrictions} of $f$.This will help to split boolean formulas in simpler ones.
\end{definition}

In ROBDD $B_f$ representing a Boolean formula $f$, we need an algorithm
restrict such that the call $restrict(0, x,B_f)$ computes the reduced
OBDD representing $f[0/x]$ using the same variable ordering as $B_f$. For each node $n$ labelled
with $x$, incoming edges are redirected to $lo(n)$ and $n$ is removed. Then, we
call reduce on the resulting OBDD. The call restrict $(1, x,B_f)$ proceeds
similarly, only we now redirect incoming edges to $hi(n)$, where for any non-terminal node $n$ in a BDD, we define $lo(n)$ to be the node pointed to via the dashed line from $n$. Dually, $hi(n)$ is the node pointed to via the solid line from $n$.
