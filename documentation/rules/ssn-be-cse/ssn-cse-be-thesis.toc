\contentsline {chapter}{ABSTRACT}{iii}{dummy.1}
\contentsline {chapter}{LIST OF TABLES}{vi}{dummy.3}
\contentsline {chapter}{LIST OF FIGURES}{vii}{dummy.5}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.7}
\contentsline {section}{\numberline {1.1}Boolean Functions}{1}{section.8}
\contentsline {section}{\numberline {1.2}Binary Decision Tree}{2}{section.9}
\contentsline {chapter}{\numberline {2}Binary Decision diagrams}{5}{chapter.13}
\contentsline {section}{\numberline {2.1}Ordered Binary Decision Diagram}{10}{section.22}
\contentsline {subsection}{\numberline {2.1.1}Reduced Ordered Binary Decision Diagram}{11}{subsection.25}
\contentsline {subsection}{\numberline {2.1.2}The Impact of the Chosen Variable Ordering}{13}{subsection.31}
\contentsline {chapter}{\numberline {3}Literature Survey}{16}{chapter.34}
\contentsline {chapter}{\numberline {4}Algorithms for BDD}{20}{chapter.37}
\contentsline {section}{\numberline {4.1}Shannon Expansion}{20}{section.38}
\contentsline {section}{\numberline {4.2}If-Then-Else Normal Form}{21}{section.39}
\contentsline {section}{\numberline {4.3}Examples}{22}{section.40}
\contentsline {section}{\numberline {4.4}The algorithm reduce}{23}{section.43}
\contentsline {section}{\numberline {4.5}The Algorithm apply}{25}{section.46}
\contentsline {section}{\numberline {4.6}The Algorithm restrict}{25}{section.50}
\contentsline {chapter}{\numberline {5}Problem Definition and Proposed System}{27}{chapter.52}
\contentsline {section}{\numberline {5.1}Nondeterministic Finite Automata}{27}{section.53}
\contentsline {subsection}{\numberline {5.1.1}Applications of NFA}{30}{subsection.57}
\contentsline {subsection}{\numberline {5.1.2}NFA determinisation and DFA minimization}{30}{subsection.58}
\contentsline {section}{\numberline {5.2}System of Communicating Automata}{32}{section.62}
\contentsline {section}{\numberline {5.3}Lamport Diagrams}{35}{section.70}
\contentsline {section}{\numberline {5.4}Problem Definition}{39}{section.73}
\contentsline {subsection}{\numberline {5.4.1}Detailed design}{39}{subsection.74}
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{42}{chapter.76}
\contentsline {chapter}{\numberline {A}Infinite Mixture Models}{43}{appendix.77}
\contentsline {section}{\numberline {A.1}Verb Clustering}{44}{section.78}
\contentsline {section}{\numberline {A.2}Metaphor Detection}{44}{section.79}
\contentsline {section}{\numberline {A.3}Criticism}{44}{section.80}
\contentsline {subsection}{\numberline {A.3.1}Limitations}{44}{subsection.81}
\contentsline {subsection}{\numberline {A.3.2}Extension}{45}{subsection.82}
\contentsline {chapter}{\numberline {B}Infinite Mixture Models}{46}{appendix.83}
\contentsline {section}{\numberline {B.1}Verb Clustering}{47}{section.84}
\contentsline {section}{\numberline {B.2}Metaphor Detection}{47}{section.85}
\contentsline {section}{\numberline {B.3}Criticism}{47}{section.86}
\contentsline {subsection}{\numberline {B.3.1}Limitations}{47}{subsection.87}
\contentsline {subsection}{\numberline {B.3.2}Extension}{48}{subsection.88}
