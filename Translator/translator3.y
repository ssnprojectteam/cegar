	%code requires
	{
		struct Block
		{
			char formula[1000];
			char type[50];
			char condition[1000];
			char firstMessage[1000];
			char previousMessage[1000];
			int count;
			struct Block *firstChild;
			struct Block *next;
			struct Block *follows;
		};
	}

%{
	#include<stdio.h>
	#include<stdlib.h>
	#include<string.h>
	#include "y.tab.h"
		
	int yylex(void);
	void yyerror(const char* s);
	
	int totalMessageCount = 0;
	int messageNo=1,conditionNo=1;
	char formula[1000];
	char condition[50];
	

	
	struct Block *makeBlock(char formula[1000], char type[50], char condition[1000], char firstMessage[1000], char previousMessage[1000], int count, struct Block *firstChild, struct Block *next, struct Block *follows)
	{
	
		struct Block *newBlock = (struct Block *) malloc (sizeof(struct Block));
		if(newBlock)
		{
			strcpy(newBlock->formula,formula);
			strcpy(newBlock->type,type);
			strcpy(newBlock->condition,condition);
			strcpy(newBlock->firstMessage,firstMessage);
			strcpy(newBlock->previousMessage,previousMessage);
			newBlock->count = count;
			newBlock->firstChild = firstChild;
			newBlock->next = next;
			newBlock->follows = follows;
		}
		else
			printf("NULLLLL %d\n",__LINE__);

		return newBlock;
	}

	void assignNext(struct Block *a, struct Block *b){
		if(a)
			a->next = b;
		else
			printf("IT'S NULL %d\n",__LINE__);
	}
	void assignFollows(struct Block *a, struct Block *b){
		if(a)
			a->follows = b;
		else
			printf("IT'S NULL %d\n",__LINE__);
	}
	
	void printBlock(struct Block *b)
	{
		//if(b)
		//	printf("%s\n",b->formula);
		if(b)
			printf("X ( %s )\n",b->formula);
		//else
		//	printf("IT'S NULL\n");
	}
	char* getFormula(struct Block *b)
	{
		return b->formula;
	}
	char* getCondition(struct Block *b)
	{
		return b->condition;
	}
	char* getPreviousMessage(struct Block *b)
	{
		return b->previousMessage;
	}
	char* getFirstMessage(struct Block *b)
	{
		return b->firstMessage;
	}
	struct Block* getNext(struct Block *b)
	{
		return b->next;
	}
	struct Block* getFollows(struct Block *b)
	{
		return b->follows;
	}
	void makeFormula(struct Block *b)
	{
		if(b) 
		{
			if(strcmp(b->type, "msg") == 0)
			{
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & X ( ");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
			}
			
			else if(strcmp(b->type, "altMessage") == 0)
			{
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & X ( ");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
			}
			
			else if(strcmp(b->type,"loop") == 0)
			{
				strcpy(b->formula," ( ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) U ! ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," )");
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & F (");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
				strcat(b->formula," )");
			}
					
			else if(strcmp(b->type,"alt") == 0)
			{
				struct Block* temp;
				strcpy(b->formula," ( ( ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) -> (");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) ");
				if(b->follows)
				{
					strcat(b->formula," & X (");
					strcat(b->formula,b->follows->formula);
					strcat(b->formula," )");
				}
				strcat(b->formula," ) ");
				temp=b->next;
				while (temp)
				{
					strcat(b->formula," & (");
					strcat(b->formula,temp->formula);
					if(b->follows)
					{
						strcat(b->formula," & X (");
						strcat(b->formula,b->follows->formula);
						strcat(b->formula," )");
					}
					strcat(b->formula," ) )");
					temp=temp->next;
				}
				strcat(b->formula," )");
				
			}
			
			else if(strcmp(b->type,"else") == 0)
			{
				strcpy(b->formula," ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) -> ( ");
				strcat(b->formula,b->firstChild->formula);
			}
			
			else if(strcmp(b->type,"onlyMessage") == 0)
			{
				printf("\nONLYMessage %s\n",b->firstChild->formula);
				strcpy(b->formula," ( ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) U ! ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," )");
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & F (");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
				strcat(b->formula," )");
			}	
			else if(strcmp(b->type,"loopMiddle") == 0)
			{
				printf("\nLOOPMiddle %s\n",b->firstChild->formula);
				//first message
				if(strcmp(b->firstChild->previousMessage,"")==0)
				{
					printf("\nFirst Message %s\n",b->firstChild->formula);
					strcpy(b->formula," ( ");
					strcat(b->formula,b->firstChild->formula);
					strcat(b->formula," ) & ( ");
					strcat(b->formula,b->next->formula);
					strcat(b->formula," ) U ! ( ( ");
					strcat(b->formula,b->firstChild->condition);
					strcat(b->formula," ) ");
					struct Block *temp=b->next->next;
					if(temp)
					{
						strcat(b->formula," & (");
						strcat(b->formula,temp->formula);
						strcat(b->formula," )");
					}
					strcat(b->formula," ) ");
				}
				else //middle message
				{
					strcpy(b->formula," ( ( ");
					strcat(b->formula,b->firstChild->previousMessage);
					strcat(b->formula," ) -> X ( ");
					strcat(b->formula,b->firstChild->formula);
					strcat(b->formula," ) ) & ");
					strcat(b->formula,b->next->formula);
				}
			}
			else if(strcmp(b->type,"loopLast") == 0)
			{
				printf("\nLOOPLast %s\n",b->firstChild->formula);
				strcpy(b->formula," ( ( ( ");
				strcat(b->formula,b->firstChild->previousMessage);
				strcat(b->formula," ) -> X ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) ) & ( ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) -> X ( ( ");
				strcat(b->formula,b->firstChild->condition);
				strcat(b->formula," ) -> ( ");
				strcat(b->formula,b->firstChild->firstMessage);
				strcat(b->formula," ) ) ) ) ");
			}			
		}
		else
			printf("B is NULL\n");
	}
	
	
	
%}

%token MESSAGE LOOP ALT CONDITION END ELSE
%start program
%union {
  struct Block * tVal;
  char * string;
  }

%type <tVal> block message mblock altblock loopblock loop alt elseblock
%%


program:	block	{
						printf("\n");
						printBlock($1);
					}

block:		mblock	{
						printf("\n block: mblock");
						$$=$1;
					}
		|	'\n' mblock	{
						printf("\n block: \\n mblock");
						$$=$2;
							}
		|	loop{
						printf("\n block: loop");
						$$=$1;
				}
		|	'\n' loop	{
						printf("\n block: \\n loop");
						$$=$2;
						}
		|   alt	{
						printf("\n block: alt");
						$$=$1;
				}
		|	'\n' alt	{
						printf("\n block: \\n alt");
						$$=$2;
						}

message: MESSAGE	{
						$$ = makeBlock($<tVal->formula>1,"msg",$<tVal->condition>1,$<tVal->firstMessage>1,$<tVal->previousMessage>1,0,NULL,NULL,NULL);
						makeFormula($$);
						printf("\n* message: MESSAGE %s",$<tVal->formula>1);
						printf("\n condition: %s",$<tVal->condition>1);
						printf("\n firstMessage: %s",$<tVal->firstMessage>1);
						printf("\n previousMessage: %s\n",$<tVal->previousMessage>1);
	
					}
						
mblock:	message {
					printf("\nmblock: message");
					$$=$1;
				}	
		|	message block	{
						printf("\nmblock: message block");
								$$ = makeBlock(getFormula($1),"msg","","","",0,NULL,$2,NULL);
								makeFormula($$);
								//printBlock($$);
							}	

		
loop:		LOOP CONDITION message loopblock END 	{ 
											printf("\n loop: LOOP CONDITION message loopblock END");
											$$ = makeBlock("","loopMiddle","","","",0,$3,$4,NULL);
											makeFormula($$);
											printBlock($$);
										}
		|	LOOP CONDITION message loopblock END block 	{
											printf("\n loop: LOOP CONDITION message loopblock END block");
											assignNext($4,$6);
											$$ = makeBlock("","loopMiddle","","","",0,$3,$4,NULL);
											makeFormula($$);
											printBlock($$);
											}
		|	LOOP CONDITION message END 	{ 
											printf("\n loop: LOOP CONDITION message END");
											char n[50];
											sprintf(n,"%s",$<tVal->formula>2);
											$$ = makeBlock(getFormula($3),"onlyMessage",n,"","",0,$3,NULL,NULL);
											makeFormula($$);
											printBlock($$);
										}
		|	LOOP CONDITION message END block 	{
											printf("\n loop: LOOP CONDITION message END block");
											char n[50];
											sprintf(n,"%s",$<tVal->formula>2);
											$$ = makeBlock(getFormula($3),"onlyMessage",n,"","",0,$3,$5,NULL);
											makeFormula($$);
											printBlock($$);

											}	
											
loopblock:	message {
					printf("\n loopblock: message");
					$$ = makeBlock("","loopLast","","","",0,$1,NULL,NULL);
					makeFormula($$);
					printBlock($$);
					}	
		|	message loopblock	{
					printf("\n loopblock: message loopblock");
					$$ = makeBlock("","loopMiddle","","","",0,$1,$2,NULL);
					makeFormula($$);
					printBlock($$);
					}
					
alt:		ALT CONDITION altblock END { 
					printf("\n alt: ALT CONDITION altblock END");
					$$ = makeBlock("","alt",getCondition($3),"","",0,$3,NULL,NULL);
					makeFormula($$);
					printBlock($$);
									}
		|	ALT CONDITION altblock END block 	{
					printf("\n alt: ALT CONDITION altblock END block");
					$$ = makeBlock("","alt",getCondition($3),"","",0,$3,NULL,$5);
					makeFormula($$);
					printBlock($$);
											}
		|	ALT CONDITION altblock elseblock END 	{
					printf("\n alt: ALT CONDITION altblock elseblock END");
					$$ = makeBlock("","alt",getCondition($3),"","",0,$3,$4,NULL);
					makeFormula($$);
					printBlock($$);
												}
		|	ALT CONDITION altblock elseblock END block {
					printf("\n alt: ALT CONDITION altblock elseblock END block");
					assignFollows($4,$6);
					printf("\n getFollows: ");					
					printBlock(getFollows($4));
					$$ = makeBlock("","alt",getCondition($3),"","",0,$3,$4,$6);
					makeFormula($$);
					printBlock($$);
													}
		
elseblock:	ELSE CONDITION altblock	{ 
					printf("\n elseblock: ELSE CONDITION altblock");
					printf("\n getFollows: ");					
					printBlock(getFollows($$));
					$$ = makeBlock("","else",getCondition($3),"","",0,$3,NULL,NULL);
					makeFormula($$);
					printBlock($$);
									}
		|	ELSE CONDITION altblock elseblock 	{
					printf("\n elseblock: ELSE CONDITION altblock elseblock");
					$$ = makeBlock("","else",getCondition($3),"","",0,$3,$4,NULL);
					makeFormula($$);
					printBlock($$);
									}
											
altblock:	message {
					printf("\n altblock: message");
					$$ = makeBlock(getFormula($1),"altMessage",getCondition($1),"","",0,$1,NULL,NULL);
					makeFormula($$);
					printBlock($$);
					}	
		|	message altblock	{
					printf("\n altblock: message altblock");
					$$ = makeBlock(getFormula($1),"altMessage",getCondition($1),"","",0,$1,$2,NULL);
					makeFormula($$);
					printBlock($$);
							}
%%

void yyerror(const char *s){
	fprintf(stderr,"%s\n",s);
	return;
}

yywrap(){
	return (1);
}

int main(void){
	formula[0]='\0';
	FILE *fp; 
	fp=fopen("msg_file","w");
	yyparse();
	fclose(fp);
	printf("%s",formula);
	return 0;
}




