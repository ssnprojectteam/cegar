Trace Description: LTL Counterexample
Trace Type: Counterexample
  -> State: 1.1 <-
    req_quote = 0
    req_bill = 0
    bill = 0
    quote = 0
    accept = 0
    reject = 0
    modify = 0
    req_mod_bill = 0
    numloops = 0
    choice = Modify
    b.loc = b0
    m.loc = m0
    s.loc = s0
  -> State: 1.2 <-
    req_quote = 1
    b.loc = b1
  -> State: 1.3 <-
    req_quote = 0
    req_bill = 1
    s.loc = s1
  -> State: 1.4 <-
    req_bill = 0
    bill = 1
    m.loc = m1
  -> State: 1.5 <-
    bill = 0
    quote = 1
    m.loc = m0
    s.loc = s2
  -> State: 1.6 <-
    quote = 0
    modify = 1
    numloops = 1
    b.loc = b4
  -> State: 1.7 <-
    modify = 0
    req_mod_bill = 1
    b.loc = b1
    s.loc = s1
  -> State: 1.8 <-
    bill = 1
    req_mod_bill = 0
    m.loc = m1
  -> State: 1.9 <-
    bill = 0
    quote = 1
    m.loc = m0
    s.loc = s2
  -> State: 1.10 <-
    quote = 0
    modify = 1
    numloops = 2
    b.loc = b4
  -> State: 1.11 <-
    modify = 0
    req_mod_bill = 1
    b.loc = b1
    s.loc = s1
  -> State: 1.12 <-
    bill = 1
    req_mod_bill = 0
    m.loc = m1
  -> State: 1.13 <-
    bill = 0
    quote = 1
    m.loc = m0
    s.loc = s2
  -> State: 1.14 <-
    quote = 0
    reject = 1
    choice = Reject
    b.loc = b3
  -- Loop starts here
  -> State: 1.15 <-
    reject = 0
  -> State: 1.16 <-