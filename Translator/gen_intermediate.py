import json

f = open("trace.txt")
t = open("inter",'w')

msg_dict = json.load(open("mapped_msgs",'r'))
hasloop = 0

msgs = f.read().split('\n')

for line in msgs:
	if '= 1' in line:
		msg = line.split('=')[0].strip()
		if msg in msg_dict:
			t.write(msg_dict[msg]+'\n')
			if hasloop == 1:
				t.write('loop infinite\n')
				hasloop = 2
	if 'Loop' in line:
		hasloop = 1

if hasloop != 0:
	t.write('end')

f.close()
t.close()

            
        
