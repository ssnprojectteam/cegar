%{
	#include<stdio.h>
	#include<stdlib.h>
	#include<string.h>
	#include "y.tab.h"
		
	int yylex(void);
	void yyerror(const char* s);
	
	int totalMessageCount = 0;
	int messageNo=1,conditionNo=1;
	char formula[1000];
	
	struct Block
	{
		char formula[1000];
		char type[10];
		char condition[1000];
		int count;
		struct Block *firstChild;
		struct Block *next;
	};
	
	struct Block *makeBlock(char formula[1000], char type[10], char condition[1000], int count, struct Block *firstChild, struct Block *next)
	{
	
		struct Block *newBlock = (struct Block *) malloc (sizeof(struct Block));
		if(newBlock)
		{
			strcpy(newBlock->formula,formula);
			strcpy(newBlock->type,type);
			strcpy(newBlock->condition,condition);
			newBlock->count = count;
			newBlock->firstChild = firstChild;
			newBlock->next = next;
		}
		else
			printf("NULLLLL %d\n",__LINE__);

		return newBlock;
	}

	void assignNext(struct Block *a, struct Block *b){
		if(a)
			a->next = b;
		else
			printf("IT'S NULL %d\n",__LINE__);
	}
	
	void printBlock(struct Block *b)
	{
		//if(b)
		//	printf("%s\n",b->formula);
		if(b)
			printf("X ( %s )\n",b->formula);
		//else
		//	printf("IT'S NULL\n");
	}
	
	void makeFormula(struct Block *b)
	{
		if(b) 
		{
			if(strcmp(b->type, "msg") == 0)
			{
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & X ( ");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
			}
			
			else if(strcmp(b->type,"loop") == 0)
			{
				strcpy(b->formula," ( ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) U ! ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," )");
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & F (");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
				strcat(b->formula," )");
			}
					
			else if(strcmp(b->type,"alt") == 0)
			{
				strcpy(b->formula," ( ( ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) -> (");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) )");
				if(b->firstChild->next)
				{
					strcat(b->formula," & (");
					strcat(b->formula,b->firstChild->next->formula);
					strcat(b->formula," )");
				}
				struct Block *temp=b->next;
				if(temp)
				{
					strcat(b->formula," & F (");
					strcat(b->formula,temp->formula);
					strcat(b->formula," )");
				}
				strcat(b->formula," )");
				
			}
			
			else if(strcmp(b->type,"else") == 0)
			{
				strcpy(b->formula," ( ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) -> ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) )");
				if(b->firstChild->next)
				{
					strcat(b->formula," & ( ");
					strcat(b->formula,b->firstChild->next->formula);
					strcat(b->formula," )");
				}
			}
		}
		else
			printf("B is NULL\n");
	}
	
	
	
%}

%token MESSAGE LOOP ALT CONDITION END ELSE
%start program
%union {
  struct Block * tVal;
  char * string;
  }

%type <tVal> block message loop alt elseblock
%%


program:	block	{
						printBlock($1);
					}

block:		message	{
						$$ = $1;
					}
		|	'\n' message	{
								$$ = $2;
							}
		|	loop{
					$$ = $1;
				}
		|	'\n' loop	{
							$$ = $2;
						}
		|   alt	{
					$$ = $1;;
				}
		|	'\n' alt	{
							$$ = $2;
						}
		
message:	MESSAGE {
						char n[100];
						strcpy(n," ( ");
						printf("%s",$<string>1);
						strcat(n,$<string>1);
						strcat(n," = 1 )");
						$$ = makeBlock(n,"msg","",0,NULL,NULL);
						makeFormula($$);
					}	
		|	MESSAGE block	{
								char n[100];
								strcpy(n,"( ");
								strcat(n,$<string>1);
								strcat(n," = 1 )");
								$$ = makeBlock(n,"msg","",0,NULL,$2);
								makeFormula($$);
							}	

		
loop:		LOOP CONDITION block END 	{ 
											char n[10];
											sprintf(n,"%s",$<string>2);
											$$=makeBlock("","loop",n,0,$3,NULL);
											makeFormula($$);
										}
		|	LOOP CONDITION block END block 	{
												char n[10];
												sprintf(n,"%s",$<string>2);
												$$=makeBlock("","loop",n,0,$3,$5);
												makeFormula($$);
											}
			
alt:		ALT CONDITION block END { 
										char n[10];
										sprintf(n,"%s",$<string>2);
										$$=makeBlock("","alt",n,0,$3,NULL);
										makeFormula($$);
									}
		|	ALT CONDITION block END block 	{
												char n[10];
												sprintf(n,"%s",$<string>2);
												$$=makeBlock("","alt",n,0,$3,$5);
												makeFormula($$);
											}
		|	ALT CONDITION block elseblock END 	{
													char n[10];
													sprintf(n,"%s",$<string>2);
													$$=makeBlock("","alt",n,0,$3,NULL);
													assignNext($3,$4);
													makeFormula($$);
												}
		|	ALT CONDITION block elseblock END block {
														char n[10];
														sprintf(n,"%s",$<string>2);
														$$=makeBlock("","alt",n,0,$3,$6);
														assignNext($3,$4);
														makeFormula($$);
													}
		
elseblock:	ELSE CONDITION block	{ 
										char n[10];
										sprintf(n,"%s",$<string>2);
										$$=makeBlock("","else",n,0,$3,NULL);
										makeFormula($$);
									}
		|	ELSE CONDITION block elseblock 	{ 
												char n[10];
												sprintf(n,"%s",$<string>2);
												$$=makeBlock("","else",n,0,$3,NULL);
												assignNext($3,$4);
												makeFormula($$);
											}
%%

void yyerror(const char *s){
	fprintf(stderr,"%s\n",s);
	return;
}

yywrap(){
	return (1);
}

int main(void){
	formula[0]='\0';
	FILE *fp; 
	fp=fopen("msg_file","w");
	yyparse();
	fclose(fp);
	printf("%s",formula);
	return 0;
}




