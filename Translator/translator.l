%{
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include "y.tab.h"
	extern YYSTYPE yylval;
	void yyerror(char *);
	char condition[1000];
	char previousMessage[1000];
	char firstMessage[1000];
	int loopJustStarted = 0;
	int insideLoop = 0;
	int insideAlt = 0;
	
%}

letter [A-Za-z_]
arrow ("->"|"-->"|"->>"|"-->>")
colon ":"
space [ \t]
msg {space}*{letter}+{arrow}{letter}+{colon}{space}?{letter}+
loop "loop"
alt "alt"
end "end"
else "else"
condition {letter}+"="{letter}+[\n]?

%%

{msg}	{
			FILE *f;
			f = fopen("msg_file","a");
			fprintf(f,"%s\n",yytext);
			fclose(f);
			
			int i,j,len;
			for(len=0;yytext[len]!='\0';len++);
			
			for(i=len-1;i>=0;i--){
			
				if(yytext[i]==':'){
					
					if(yytext[i+1]==' ')
						i++;
					break;
				}
			}
			
			yylval.string = strdup(yytext+i+1);
			strcpy((yylval.tVal)->formula," ( ");
			strcat((yylval.tVal)->formula,(yytext+i+1));
			strcat((yylval.tVal)->formula," = 1 )");
			
			if(insideLoop)
			{
				strcpy((yylval.tVal)->condition," ( ");
				strcat((yylval.tVal)->condition,condition);
				strcat((yylval.tVal)->condition," ) ");
				
				strcpy((yylval.tVal)->previousMessage,previousMessage);
				strcpy(previousMessage,(yylval.tVal)->formula);
				
				strcpy((yylval.tVal)->firstMessage,firstMessage);
				if(loopJustStarted)
				{
					strcpy(firstMessage,(yylval.tVal)->formula);
					loopJustStarted=0;
				}
			}
			if(insideAlt)
			{
				strcpy((yylval.tVal)->condition," ( ");
				strcat((yylval.tVal)->condition,condition);
				strcat((yylval.tVal)->condition," ) ");
				
			}
			
			return MESSAGE;
		}
{space} ;
{loop} {
			loopJustStarted = 1;
			insideLoop = 1;
			strcpy(previousMessage,"");
			return LOOP;
		}
{alt} {
			insideAlt = 1;
			return ALT;
		}
{end} {
			strcpy(condition,"");
			strcpy(firstMessage,"");
			strcpy(previousMessage,"");
			insideLoop = 0;
			insideAlt = 0;
			return END;
		}
{else} {return ELSE;}
{condition} {			
				int i,j,len;
				for(len=0;yytext[len]!='\0';len++);
				if(yytext[len-1]=='\n')
					yytext[len-1]='\0';
				//yylval.string = strdup(yytext);
				strcpy(condition,(yytext));
				strcpy((yylval.tVal)->formula,(yytext));
				return CONDITION;}
\n 	  ;
.         { char new[30];
			//sprintf(new,"%c Invalid Character",*yytext);
			sprintf(new," ");
			yyerror(new);}

%%
