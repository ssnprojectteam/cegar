%{
	#include<stdio.h>
	#include<stdlib.h>
	#include<string.h>
	#include "y.tab.h"
		
	int yylex(void);
	void yyerror(const char* s);
	
	int totalMessageCount = 0;
	int messageNo=1,conditionNo=1;
	char formula[1000];
	
	struct Block
	{
		char formula[1000];
		char type[10];
		char condition[1000];
		int count;
		struct Block *firstChild;
		struct Block *next;
	};
	
	struct Block *makeBlock(char formula[1000], char type[10], char condition[1000], int count, struct Block *firstChild, struct Block *next)
	{
	
		struct Block *newBlock = (struct Block *) malloc (sizeof(struct Block));
		if(newBlock)
		{
			strcpy(newBlock->formula,formula);
			strcpy(newBlock->type,type);
			strcpy(newBlock->condition,condition);
			newBlock->count = count;
			newBlock->firstChild = firstChild;
			newBlock->next = next;
		}
		else
			printf("NULLLLL %d\n",__LINE__);

		return newBlock;
	}

	void assignNext(struct Block *a, struct Block *b){
		if(a)
			a->next = b;
			else
			printf("IT'S NULL %d\n",__LINE__);
	}
	
	void printBlock(struct Block *b)
	{
		if(b)
			printf("X ( %s )\n",b->formula);
		else
			printf("IT'S NULL\n");
	}
	
	void makeFormula(struct Block *b)
	{
		if(b) 
		{
			if(strcmp(b->type, "block") == 0)
			{
				struct Block *temp=b->firstChild;
				b->count=temp->count;
				strcpy(b->formula,temp->formula);
				temp=temp->next;
				while(temp!=NULL)
				{
					strcat(b->formula," & (");
					int i=0;
					while(i<b->count+1)
					{
						strcat(b->formula," X (");
						i++;
					}
					b->count++;
					strcat(b->formula," ");
					strcat(b->formula,temp->formula);
					i=0;
					while(i<b->count)
					{
						strcat(b->formula," )");
						i++;
					}
					strcat(b->formula," )");
					
					temp=temp->next;
				}
			}
			else if(strcmp(b->type,"loop") == 0)
			{
				
				strcpy(b->formula," ( F ( ! ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) ) & ( ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) U ! ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) ) )");
			}
			
			
			
			else if(strcmp(b->type,"alt") == 0)
			{
				strcpy(b->formula," ( ( ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) -> ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) )");
				if(b->firstChild->next)
				{
					strcat(b->formula," & ( ");
					strcat(b->formula,b->firstChild->next->formula);
					strcat(b->formula," )");
				}
				strcat(b->formula," )");
			}
			
			else if(strcmp(b->type,"else") == 0)
			{
				strcpy(b->formula," ( ( ");
				strcat(b->formula,b->condition);
				strcat(b->formula," ) -> ( ");
				strcat(b->formula,b->firstChild->formula);
				strcat(b->formula," ) )");
				if(b->firstChild->next)
				{
					strcat(b->formula," & ( ");
					strcat(b->formula,b->firstChild->next->formula);
					strcat(b->formula," )");
				}
			}
		}
		else
			printf("B is NULL\n");
	}
	
	
	
%}

%token MESSAGE LOOP ALT CONDITION END ELSE
%start program
%union {
  struct Block * tVal;
  char * string;
  }

%type <tVal> block message loop alt elseblock
%%


program: 
         /* empty */ 
		| program block { printBlock($2);	}

block:
			message	{
						$$ = $1;
						//printBlock($$);
					}
		|	block message	{
		
								$$ = makeBlock("","block","",0,$1,NULL);
								assignNext($1,$2);
								makeFormula($$);
								//printBlock($$);
							}
		|	block loop		{
		
								$$ = makeBlock("","block","",0,$1,NULL);
								assignNext($1,$2);
								makeFormula($$);
								//printBlock($$);
							}
		|   block alt		{
		
								$$ = makeBlock("","block","",0,$1,NULL);
								assignNext($1,$2);
								makeFormula($$);
								//printBlock($$);
							}
		| block '\n'

message:
			MESSAGE {
			
							char n[100];
							strcpy(n,"( ");
							strcat(n,$<string>1);
							strcat(n," = 1 )");
							//sprintf(n,"%s",$<string>1);
							//sprintf(n,"( A%d )",messageNo);
							$$ = makeBlock(n,"msg","",0,NULL,NULL);
							//printBlock($$);
							messageNo++;
								
			}		
loop:
		LOOP CONDITION block END { 
									char n[10];
									sprintf(n,"%s",$<string>2);
									//sprintf(n,"( C%d )",conditionNo);
									$$=makeBlock("","loop",n,0,$3,NULL);
									conditionNo++;
									makeFormula($$);
								}
			
alt:	
		ALT CONDITION block elseblock END { 
									char n[10];
									sprintf(n,"%s",$<string>2);
									//sprintf(n,"( C%d )",conditionNo);
									$$=makeBlock("","alt",n,0,$3,NULL);
									assignNext($3,$4);
									conditionNo++;
									makeFormula($$);
								}
		
elseblock:	
			ELSE CONDITION block { 
									char n[10];
									//sprintf(n,"( C%d )",conditionNo);
									sprintf(n,"%s",$<string>2);
									$$=makeBlock("","else",n,0,$3,NULL);
									conditionNo++;
									makeFormula($$);
								}
		|	ELSE CONDITION block elseblock { 
									char n[10];
									//sprintf(n,"( C%d )",conditionNo);
									sprintf(n,"%s",$<string>2);
									$$=makeBlock("","else",n,0,$3,NULL);
									assignNext($3,$4);
									conditionNo++;
									makeFormula($$);
								}
%%

void yyerror(const char *s){
	fprintf(stderr,"%s\n",s);
	return;
}

yywrap(){
	return (1);
}

int main(void){
	formula[0]='\0';
	FILE *fp; 
	fp=fopen("msg_file","w");
	
	yyparse();
	fclose(fp);
	printf("%s",formula);
	return 0;
}




